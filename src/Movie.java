import java.time.LocalDate;

public class Movie {
    private String movieName;
    private MovieType movieGenre;
    private LocalDate releaseDate;
    private String authorSurname;

    public Movie(String movieName, MovieType movieGenre, LocalDate releaseDate, String authorSurname) {
        this.movieName = movieName;
        this.movieGenre = movieGenre;
        this.releaseDate = releaseDate;
        this.authorSurname = authorSurname;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public MovieType getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(MovieType movieGenre) {
        this.movieGenre = movieGenre;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    @Override
    public String toString() {
        return "Movie: " +
                "name = '" + movieName + '\'' +
                ", genre = " + movieGenre +
                ", release date = " + releaseDate +
                ", author's surname = '" + authorSurname + '\'' + '\'' ;
    }
}
