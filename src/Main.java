import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        MoviesDatabase bazaFilmow = new MoviesDatabase();
        System.out.println("Wpisz komende: dodaj_film, wypisz_wszystkie, wypisz_gatunek, quit");
        String komenda = sc.nextLine();

        do{
            if (komenda.equals("quit")){break;}
            else if(komenda.equals("dodaj_film")){
                System.out.println("Wpisz nazwe filmu:");
                String nazwa = sc.nextLine();
                System.out.println("Wpisz gatunek filmu (ACTION, DRAMA, COMEDY, HORROR).");
                MovieType gatunek = MovieType.valueOf(sc.nextLine().toUpperCase());
                System.out.println("Wpisz date wydania (dd-mm-yyyy):");
                String dataUnformatted = sc.nextLine();
                try {
                    LocalDate dataWydania = LocalDate.parse(dataUnformatted, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                System.out.println("Wpisz nazwisko autora:");
                String nazwiskoAutora = sc.nextLine();
                bazaFilmow.addMovie(new Movie(nazwa,gatunek,dataWydania,nazwiskoAutora));
                System.out.println("Film zostal dodany.");
                }catch (DateTimeParseException dtpe){
                    System.out.println("Zly format daty.");
                }
                System.out.println("Wpisz komende: dodaj_film, wypisz_wszystkie, wypisz_gatunek, quit");
            }
            else if(komenda.equals("wypisz_wszystkie")){
                bazaFilmow.printAllMovies();
                System.out.println("Wpisz komende: dodaj_film, wypisz_wszystkie, wypisz_gatunek, quit");
            }
            else if(komenda.equals("wypisz_gatunek")){
                System.out.println("Podaj gatunek jaki chcesz wypisac (ACTION, DRAMA, COMEDY, HORROR): ");
                MovieType gatunek = MovieType.valueOf(sc.nextLine().toUpperCase());
                bazaFilmow.printAllMovies(gatunek);
                System.out.println("Wpisz komende: dodaj_film, wypisz_wszystkie, wypisz_gatunek, quit");
            }
            else{
                System.out.println("Wpisales zla komende.");
                System.out.println("Wpisz komende: dodaj_film, wypisz_wszystkie, wypisz_gatunek, quit");
            }
            komenda = sc.nextLine();
        } while (!komenda.equals("quit"));

        System.out.println("Koncze prace programu...");
    }
}
