import java.util.HashMap;
import java.util.Map;

public class MoviesDatabase {
    Map<String, Movie> movieMap = new HashMap<>();

    public void addMovie(Movie film){
        movieMap.put(film.getMovieName(), film);
    }

    public Movie searchMovie(String nazwa) throws Exception {
        for (Movie film : movieMap.values()){
            if (film.getMovieName().equals(nazwa)){
                return film;
            }
        }
        throw new Exception("Movie now found!");
    }

    public void printAllMovies(){
        for (Movie film : movieMap.values()){
            System.out.println(film);
        }
    }

    public void printAllMovies(MovieType movieType){
        for (Movie film : movieMap.values()){
            if (film.getMovieGenre() == movieType){
                System.out.println(film);
            }
        }
    }
}

//    Stwórz klasę MoviesDatabase która posiada:
//        - jako pole posiada mapę filmów. Mapa powinna mapować z wartości typu String
//          (będzie to nazwa filmu) na model (na obiekty klasy Movie)
//        - stwórz metodę dodawania do bazy danych filmów ( addMovie(Movie m))
//          która dodaje do mapy film
//        - stwórz metodę wyszukiwania filmów z bazy danych, która przyjmuje jako
//          parametr nazwę filmu, a zwraca film który jest w bazie danych pod tą nazwą.
//        - stwórz metodę wypisywania wszystkich filmów. Metoda powinna iterować po
//          wartościach mapy i wypisywać informacje o filmie (nadpisz metodę
//          toString w klasie ...[wiesz jakiej?]). metoda printAllMovies().
//        - stwórz metodę o tej samej nazwie co powyższa, która przyjmuje jako parametr
//          typ filmu (MovieType) i również iteruje wartości, jedak wypisuje tylko te filmy
//          których MovieType odpowiada temu podanemu jako parametr.
